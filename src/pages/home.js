import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ShowRecipe from '../components/showRecipe';
import Filter from '../components/filter';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  list: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  }
}));

const HomePage = () => {
  const classes = useStyles();
  const history = useHistory();
  let arrRecipes = JSON.parse(localStorage.getItem('recipes')) || [];
  const [tags, setTags] = useState([]);

  const handleUpdateTags = (tags) => {
    setTags(tags);
  }

  arrRecipes = arrRecipes.filter(item => {
    const arrIngs = [];
    for (let i of item.ings) {
      arrIngs.push(i.name.toLowerCase());
    }
    const filteredTags = tags.filter(tag => arrIngs.includes(tag.toLowerCase()));
    return filteredTags.length === tags.length;
  });

  return (
    <div className={classes.root}>
      <h1>Easier Chef</h1>
      <Button variant="contained" color="primary" onClick={() => history.push('/add-recipe')}>
        Add recipe
      </Button>
      <Filter tags={tags} handleUpdateTags={handleUpdateTags} />
      <div className={classes.list}>
        {arrRecipes.map((recipe, index) => <ShowRecipe key={index} recipe={{ ...recipe }} />)}
      </div>
    </div>
  )
}

export default HomePage;