import React from 'react';
import AddRecipe from '../components/addRecipe';

const AddRecipePage = () => {
  return (
    <div className='add-recipe'>
      <AddRecipe />
    </div>
  )
}

export default AddRecipePage;