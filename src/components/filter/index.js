import React from 'react';
import { InputTagsContainer } from 'react-input-tags';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    '& input': {
      width: '300px !important'
    }
  },
  title: {
    marginBottom: 10
  }
}));

const Filter = ({ tags, handleUpdateTags }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.title}>Filter by ingredients</div>
      <InputTagsContainer
        tags={tags}
        handleUpdateTags={handleUpdateTags}
      />
    </div>
  );

}

export default Filter;