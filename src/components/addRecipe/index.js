import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Typography, Box, Button } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { add } from '../../features/recipeSlice';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3)
  },
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  field: {
    margin: theme.spacing(1),
    width: 600,
  },
  layoutBottom: {
    display: 'flex'
  },
  group: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center'
  }
}));

const AddRecipe = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [recipe, setRecipe] = useState({
    dishName: '',
    ings: [],
    steps: '',
    pictureURL: ''
  });
  const [name, setName] = useState('');
  const [quantity, setQuantity] = useState('');
  const [unit, setUnit] = useState('');

  return (
    <div className={classes.root}>
      <h1 className={classes.title}>Add recipe</h1>
      <form className={classes.layout} noValidate autoComplete="off">
        <TextField
          className={classes.field}
          id=""
          label="Name of the dish"
          variant="outlined"
          onChange={(e) => setRecipe({
            ...recipe,
            dishName: e.target.value
          })}
        />
        <Typography>List of ingredients</Typography>
        {recipe.ings.map((item, index) => (
          <Typography key={index}>{item.name} - {item.quantity} {item.unit}</Typography>
        ))}
        <Box className={classes.layout}>
          <TextField
            className={classes.field}
            id=""
            label="Ingredient name"
            variant="outlined"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <TextField
            className={classes.field}
            id="" type='number'
            label="Ingredient quantity"
            variant="outlined"
            value={quantity}
            onChange={(e) => setQuantity(e.target.value)}
          />
          <TextField
            className={classes.field}
            id=""
            label="Unit of measurement for ingredient qty"
            variant="outlined"
            value={unit}
            onChange={(e) => setUnit(e.target.value)}
          />
        </Box>
        <Box>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              const newIngs = recipe.ings;
              newIngs.push({ name, quantity, unit });
              setRecipe({
                ...recipe,
                ings: newIngs
              });
              setName('');
              setQuantity('');
              setUnit('');
            }}
          >+</Button>
        </Box>
        <TextField
          className={classes.field}
          id=""
          label="Steps to cook"
          variant="outlined"
          multiline
          rows={8}
          onChange={(e) => setRecipe({
            ...recipe,
            steps: e.target.value
          })}
        />
        <TextField
          className={classes.field}
          id=""
          label="Picture URL"
          variant="outlined"
          onChange={(e) => setRecipe({
            ...recipe,
            pictureURL: e.target.value
          })}
        />
        <Button variant="contained" color="primary" onClick={() => {
          dispatch(add(recipe));
          history.push('/');
        }}>Add</Button>
      </form>
    </div>
  );
}

export default AddRecipe;