import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import HomePage from './pages/home';
import AddRecipePage from './pages/addRecipe';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/add-recipe" exact component={AddRecipePage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
