import { createSlice } from '@reduxjs/toolkit';

const savedRecipes = JSON.parse(localStorage.getItem('recipes')) || [];

export const recipeSlice = createSlice({
  name: 'recipe',
  initialState: {
    recipes: savedRecipes,
  },
  reducers: {
    add: (state, action) => {
      const newRecipes = state.recipes;
      newRecipes.push(action.payload);
      state.recipes = newRecipes;
      localStorage.setItem('recipes', JSON.stringify(newRecipes));
    },
  },
});

export const { add } = recipeSlice.actions;

export const recipes = state => state.recipe.recipes;

export default recipeSlice.reducer;
